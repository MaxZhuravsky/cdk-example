import {Component, Inject, OnInit, Optional} from '@angular/core';
import {CUSTOM_OVERLAY_DATA, LOCAL_OVERLAY_REF, LocalOverlayRef, OverlayData} from "../overlay-management.service";
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'app-overlay-example',
  templateUrl: './overlay-example.component.html',
  styleUrls: ['./overlay-example.component.css']
})
export class OverlayExampleComponent implements OnInit {

  public _beforeClose = new Subject<string>();
  public _afterClosed = new Subject<string>();

  public counter = 0;
  constructor(@Optional() @Inject(CUSTOM_OVERLAY_DATA) private overLayData: OverlayData,
              @Inject(LOCAL_OVERLAY_REF) private localRef: LocalOverlayRef) { }

  ngOnInit() {
    console.log(this.overLayData);
    this.counter = this.overLayData.counterData;
  }

  increment() {
    this.counter += 1;
  }

  closeFromInside() {
    this._beforeClose.next('closed! ' + this.counter);
    this.localRef.overlayRef.dispose();
  }
  public test() {
    console.log('test');
  }

}
