import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TargetListComponent } from './target-list/target-list.component';
import { TargetComponent } from './target/target.component';
import {OverlayModule} from "@angular/cdk/overlay";
import { OverlayExampleComponent } from './overlay-example/overlay-example.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";


@NgModule({
  declarations: [
    AppComponent,
    TargetListComponent,
    TargetComponent,
    OverlayExampleComponent
  ],
  entryComponents: [TargetListComponent, OverlayExampleComponent],
  imports: [
    BrowserModule,
    OverlayModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
