import { Component } from '@angular/core';
import {OverlayManagementService} from "./overlay-management.service";
import {TargetListComponent} from "./target-list/target-list.component";
import {OverlayExampleComponent} from "./overlay-example/overlay-example.component";






@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [OverlayManagementService]
})
export class AppComponent {
  title = 'app';
  isOpen = false;
  targetIdToScroll = null;

  public targets = [
    {id: '123', value: 'fewkjh'},
    {id: '5333r', value: 'fewkjh'},
    {id: '67532f', value: 'fewkjh'},
    {id: '43gbrhnrnwra2', value: 'fewkjh'},
    {id: 'efw', value: 'fewkjh'},
    {id: '123', value: 'fewkjh'},
    {id: '12vfehge3', value: 'fewkjh'},
    {id: 'fvrehtrh', value: 'fewkjh'},
    {id: '126545313', value: 'fewkjh'},
    {id: 'csaca', value: 'fewkjh'},
    {id: '12thr543', value: 'fewkjh'},
    {id: 'vwdf5vwr', value: 'fewkjh'},
    {id: 'wfc2147', value: 'fewkjh'},
    {id: 'dq657', value: 'fewkjh'},
    {id: 'thbr75', value: 'fewkjh'},
    {id: 'fwe4536', value: 'fewkjh'},
    {id: 'gbe674', value: 'fewkjh'},
    {id: 'ger776575', value: 'fewkjh'},
  ];

  constructor(private overlayService: OverlayManagementService) {}

  open() {
    this.isOpen = true;
  }

  openAndscroll() {
    this.isOpen = true;
    this.targetIdToScroll = 'ger776575';
  }

  openOverlay() {
    const cmp = this.overlayService.open(OverlayExampleComponent, {}, {counterData: 9, x: 10}) as OverlayExampleComponent;
    cmp._beforeClose.subscribe(data => {
      console.log(data);
    });
  }

  closeOverlay() {
    this.overlayService.close();
  }
}
