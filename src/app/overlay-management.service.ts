import {ComponentRef, Injectable, InjectionToken, Injector} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal, PortalInjector} from "@angular/cdk/portal";

export interface OverlayData {
  [id: string]: any;
}

export interface LocalOverlayRef {
  overlayRef: OverlayRef;
}

export const CUSTOM_OVERLAY_DATA = new InjectionToken<OverlayData>('CUSTOM_OVERLAY_DATA');
export const LOCAL_OVERLAY_REF = new InjectionToken<LocalOverlayRef>('LOCAL_OVERLAY_REF');


@Injectable()
export class OverlayManagementService {
  // main limitation - each component can open only 1 overlay
  private overlayRef: OverlayRef; // state full service, provide it in component level!!!

  constructor(private overlay: Overlay, private injector: Injector) { }

  public open(component, config: OverlayConfig = {}, data: OverlayData = {}) {
    this.overlayRef = this.createOverlay(config);
    const overlayComponent = this.attachOverlayContainer(data, component);

    return overlayComponent; // reference to the component
  }

  public close() {
    this.overlayRef.dispose();
  }

  private attachOverlayContainer(data: OverlayData, component: any) {
    const injector = this.createInjector(data);

    // create a portal and inject all necessary tokens(data, overlay..  .)
    const containerPortal = new ComponentPortal(component, null, injector);
    // attach the portal to the overlay
    const containerRef = this.overlayRef.attach(containerPortal);

    return containerRef.instance; // return component reference
  }


  private createOverlay(config: OverlayConfig) {
    // Returns an OverlayConfig
    const overlayConfig = this.getOverlayConfig(config);

    // Returns an OverlayRef
    return this.overlay.create(overlayConfig);
  }

  private getOverlayConfig(config: OverlayConfig): OverlayConfig {
    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    // this.overlay.position().connectedTo() connect to some element (when used as menu ...)

    const overlayConfig = new OverlayConfig({
      hasBackdrop: true,
      backdropClass: 'dark-backdrop',
      panelClass: 'tm-file-preview-dialog-panel',
      scrollStrategy: this.overlay.scrollStrategies.block(),
      positionStrategy
    });

    return overlayConfig;
  }

  private createInjector(data: OverlayData): PortalInjector {
    // Instantiate new WeakMap for our custom injection tokens
    const injectionTokens = new WeakMap();

    // Set custom injection tokens
    injectionTokens.set(LOCAL_OVERLAY_REF, { overlayRef: this.overlayRef });
    injectionTokens.set(CUSTOM_OVERLAY_DATA, { ...data }); // do not init if no data passed, optional dependency

    // Instantiate new PortalInjector
    return new PortalInjector(this.injector, injectionTokens);
  }

}
