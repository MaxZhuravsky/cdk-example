import {AfterViewInit, Component, ElementRef, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {TargetComponent} from '../target/target.component';

@Component({
  selector: 'app-target-list',
  templateUrl: './target-list.component.html',
  styleUrls: ['./target-list.component.css']
})
export class TargetListComponent implements OnInit, AfterViewInit {
  @Input() targets: any[];
  @Input() targetIdToScroll: string | null = null;
  @ViewChildren(TargetComponent/*, { read: ElementRef }*/) targetsRef: QueryList<TargetComponent>;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if (this.targetIdToScroll) {
      console.log(this.targetsRef);
      let targetRef = null;
      const arr = this.targetsRef.toArray();
      arr.map((target: TargetComponent) => {
        if (target.getId() === this.targetIdToScroll) {
          target.scroll();
        }
      });

      console.log(targetRef);
    }
  }
}
