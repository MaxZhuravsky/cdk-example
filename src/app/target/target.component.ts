import {ChangeDetectorRef, Component, ElementRef, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-target',
  templateUrl: './target.component.html',
  styleUrls: ['./target.component.css']
})
export class TargetComponent implements OnInit {
  @Input() id: string;
  @Input() value: string;
  targetScrolled = false;

  constructor(private ref: ElementRef, private cd: ChangeDetectorRef) { }

  ngOnInit() {
  }

  public getId() {
    return this.id;
  }

  public scroll() {
    this.ref.nativeElement.scrollIntoView({behavior: 'smooth'});
   this.triggerScrollAnimation();
  }

  private triggerScrollAnimation() {
    this.targetScrolled = true;
    this.cd.detectChanges();
  }

}
